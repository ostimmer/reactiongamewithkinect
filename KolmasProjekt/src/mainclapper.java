import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.BufferedWriter;
import java.io.IOException;

/**
 * Class for getting new coordinates and calculating the distance.
 * @author siim
 *
 */
public class mainclapper {
	static int leftx;
	static int lefty;
	static int rightx;
	static int righty; 
	static int headx;
	static int heady;
	static int neededDist = peaklass.needed_dist;

	/**
	 * public method for getting to know that are hands together 
	 * @return boolean
	 */
	public static boolean getStatus(){
		int dist = Math.abs(leftx - rightx) + Math.abs(lefty - righty);
		if (dist < neededDist)
		{
			return true;
		}
		return false;
	}
	
	public static boolean getStatus_lhand_head(){
		if (lefty < heady)
		{
			return true;
		}
		return false;
	}

	public static boolean getStatus_rhand_head(){
		if (righty < heady)
		{
			return true;
		}
		return false;
	}
	
	

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void run_kinect() throws IOException {
		// TODO Auto-generated method stub
		java.io.File file = new java.io.File("coordinates.txt");

		if (!file.exists())
		{
			file.createNewFile();
			System.out.println("File doesn\'t exist. Creating one.");
			FileWriter writing = new FileWriter(file);
			BufferedWriter bw = new BufferedWriter(writing);
			bw.write("0 0 1000 1000 1000 1000"); // just quite random things
			bw.close();
			return;
		}

		/**
		 * this is for making java program to open up file for some time to get new coordinates there
		 */
		try{
			java.util.Scanner sc = new java.util.Scanner(file);
			if(sc.hasNextLine()) // only if there is a line to read
			{
				String row = sc.nextLine();

				String[] parts = row.split(" ");
				leftx = Integer.parseInt(parts[0]);
				lefty = Integer.parseInt(parts[1]);
				rightx = Integer.parseInt(parts[2]);
				righty = Integer.parseInt(parts[3]);
				headx = Integer.parseInt(parts[4]);
				heady = Integer.parseInt(parts[5]);
				sc.close();
			}
		}catch(FileNotFoundException ex){
			System.out.println(ex);
			return;}
	}
}