import java.awt.*;
//import java.io.*;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.swing.*;
import java.util.Random;
public class peaklass {

	// settingud
	public static int muutumise_aeg = 1000;
	public static Color[] V�rvid = {Color.BLUE, Color.YELLOW, Color.RED, Color.GREEN, Color.PINK};
	public static int needed_dist =70;
	public static int m�ngude_arv =15;

	//kohad mida kasutatakse progis
	public static String Mis_v�rv;
	public static boolean kinect_�hendatud = false;
	public static boolean nupule_vajutatud = false;
	public static boolean l�petan_vajutatud = false;
	public static boolean koordinaaadid_muutuvad = false;
	public static boolean �petus_loetud = false;
	public static int �igeid_vastuseid = 0;
	public static int v�rv;
	public static int V�rv;

	// Kuvatav tekst (p�hm v�iks t�lkida). P�ris k�ik k�ll ei kajastu siin hektel.
	public static String M�ngu_l�petamine = "L�peta m�ng";
	public static String kuidas_m�ngida = "Pane k�ed kokkus siis, kui tausta v�rv ja v�rvi nimi on samad! M�ngu ALUSTAMISEKS t�sta parem k�si pea kohale ning l�petamiseks t�sta vasak k�si pea kohale.";
	public static String[] v�rvid = {"SININE", "KOLLANE", "PUNANE", "ROHELINE", "ROOSA"};
	public static String vajuta_siis = "Vajuta kui taust on ";
	public static String vajuta = "Vajutan";
	public static String uus_m�ng = "Uus m�ng";
	public static String l�peta_m�ng = "L�peta m�ng";
	public static String sinu_kiirus = " Suutsid reageerida ";
	public static String millisekundiga = " millisekundiga";
	public static String k�ed_l�hedal = "K�ed on liiga l�hedal";
	public static String m�ng_l�bi = "M�ng sai l�bi";
	public static String kinectita = "M�ngi Kinectita";
	public static String kin_�hendatud = "Kinect on �hendatud";
	public static String kin_�hendamata = "Kinect on �HENDAMATA";

	// genereerib suvalise numbri 0-st 4-ni
	public static int suvaline_number() {
		int tagastan;
		Random rn = new Random();
		tagastan = rn.nextInt(5);
		return tagastan;
	}

	// Kinecti �henduse kindlaks tegemiseks vajalikud muutujad
	public static int xl1 = mainclapper.leftx;
	public static int yl1 = mainclapper.lefty;
	public static int yr1 = mainclapper.righty;
	public static int xr1 = mainclapper.rightx;
	public static int yh1 = mainclapper.righty;
	public static int xh1 = mainclapper.rightx;


	//Teeb kindlaks Kinect muutuse
	public static void v�rreldav_kinecti_muutus(){
		xl1 = mainclapper.leftx;
		yl1 = mainclapper.lefty;
		//int xr1 = mainclapper.rightx;
		yr1 = mainclapper.righty;
		xr1 = mainclapper.rightx;
		xh1 = mainclapper.headx;
		yh1 = mainclapper.heady;
	}

	// kontrollib kas koordinaadid muutuvad
	public static int xl2 = mainclapper.leftx;
	public static int yl2 = mainclapper.lefty;
	public static int xr2 = mainclapper.rightx;
	public static int yr2 = mainclapper.righty;
	public static int xh2 = mainclapper.headx;
	public static int yh2 = mainclapper.heady;


	// kontrollib kas koordinaadid muutuvad
	public static void kas_koordinaadid_muutuvad(){
		xl2 = mainclapper.leftx;
		yl2 = mainclapper.lefty;
		xr2 = mainclapper.rightx;
		yr2 = mainclapper.righty;	
		xh2 = mainclapper.headx;
		yh2 = mainclapper.heady;
		if (xl2 != xl1 || xr2 != xr1 || yl2 != yl1 || yr2 != yr1 || xh2 != xh1 || yh2 != yh1 ){
			koordinaaadid_muutuvad = true;
		}
		else {
			koordinaaadid_muutuvad = false;
		}
	}


	public static void main(String[] args) throws FileNotFoundException {

		// avab kinecti skeletoni ja loob sisu coordinaatide faili
		Process p;
		try {
			p = Runtime.getRuntime().exec("SkeletonBasics-D2D.exe");
			//p.waitFor();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		// Raami loomine
		JFrame raam = new JFrame("Reaktsiooniaja m�ng"); // raami loomine
		raam.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		raam.setSize(800, 730); // 
		//Raami layout
		raam.setLocation(785, 0);
		Container sisu = raam.getContentPane(); // konteineri loomine
		sisu.setLayout(null);
		raam.setVisible(true);

		//muutjua mis suurusega on raam
		int rl = sisu.getWidth();
		int rp = sisu.getHeight();
		v�rv=suvaline_number();

		// Nuppude ja muude objektide lisamine
		JLabel n1 = new JLabel("", JLabel.CENTER);
		n1.setFont(new Font("Serif", Font.BOLD, rl/12));

		JButton n2 = new JButton(vajuta);
		n2.addActionListener(new Vajutan_kuular());
		n2.setMnemonic(java.awt.event.KeyEvent.VK_V);

		JLabel n3 = new JLabel("Alustame", JLabel.CENTER);
		n3.setFont(new Font("Serif", Font.BOLD, rp/6));

		JButton n4 = new JButton (M�ngu_l�petamine);
		n4.addActionListener(new l�petan_kuular());
		n4.setMnemonic(java.awt.event.KeyEvent.VK_L);

		JTextArea �petus = new JTextArea(kuidas_m�ngida,10, 1);
		�petus.setEditable(false);
		�petus.setLineWrap(true);

		JButton n9 = new JButton("M�ngima!");
		n9.addActionListener(new m�ngima());
		n9.setMnemonic(java.awt.event.KeyEvent.VK_M);

		JButton n10 = new JButton("Seaded");
		n10.addActionListener(new muuda_seadeid());
		n10.setMnemonic(java.awt.event.KeyEvent.VK_S);

		String kinecti_�hendus = kin_�hendamata;
		JLabel n11 = new JLabel(kinecti_�hendus, JLabel.CENTER);
		n11.setFont(new Font("Serif", Font.BOLD, rl/25));

		//Erinevate vajalike muutujate loomine
		long aeg_k�ima = 0;
		long aeg_kinni = 0;
		int vahetamise_kordi = 0;
		V�rv=suvaline_number();
		boolean kas_kuvan_aega = false;
		boolean kas_m��dan_aega = true;
		long minu_kiirus;


		// selleks et n�itaks �igesti, kas kinect on �hendatud alguses
		try {
			mainclapper.run_kinect();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		//Nuppude ja muu n�htavaks tegemine		
		sisu.add(n3);
		sisu.add(n9);
		sisu.add(n10);
		sisu.add(n11);
		sisu.add(�petus);

		sisu.setBackground(Color.WHITE);
		// avakuva ts�kkel
		while(true) {
			v�rreldav_kinecti_muutus();
			rl = sisu.getWidth();
			rp = sisu.getHeight();

			�petus.setFont(new Font("Serif", Font.BOLD, rp/16));
			�petus.setBounds(rl/10, rp/10*2, rl/10*8, rp/10*5);
			n9.setBounds(rl/10*4, rp/10*8, rl/10*2, rp/10*1);
			n10.setBounds(rl/10*6, rp/10*8, rl/10*2, rp/10*1);


			if (�petus_loetud || mainclapper.getStatus_rhand_head()&koordinaaadid_muutuvad&!mainclapper.getStatus_lhand_head()) {
				sisu.remove(�petus);
				sisu.remove(n9);
				sisu.remove(n10);
				break;
			}

			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			try {
				mainclapper.run_kinect();
			} catch (IOException e1) {
				e1.printStackTrace();
			}

			kas_koordinaadid_muutuvad();
			n11.setBounds(rl/10*1, rp/10*0, rl/10*7, rp/10*1);
			n11.setFont(new Font("Serif", Font.BOLD, rl/25));
			if(koordinaaadid_muutuvad){
				n11.setText(kin_�hendatud);
			}
			else{
				n11.setText(kin_�hendamata);
			}
		}


		// aitab kontrollinda kas kinect on �hendatud
		try {
			mainclapper.run_kinect();
		} catch (IOException e) {
			e.printStackTrace();
		}

		/// kirjutab viimase m�ngu faili
		java.io.PrintWriter pw = new java.io.PrintWriter("tulemus.txt");
		java.util.Calendar m�ng_l�bi_aeg = java.util.Calendar.getInstance();

		//korduv peats�kkel (uus m�ng)
		while(true){
			//muutjua mis suurusega on raam
			rl = sisu.getWidth();
			rp = sisu.getHeight();
			v�rv=suvaline_number();

			aeg_k�ima = 0;
			aeg_kinni = 0;
			vahetamise_kordi = 0;
			V�rv=suvaline_number();
			kas_kuvan_aega = false;
			kas_m��dan_aega = true;		

			//aja k�ima panek
			java.util.Calendar prog_k�ima1 = java.util.Calendar.getInstance();
			long prog_k�ima = prog_k�ima1.getTimeInMillis();

			sisu.add(n1);
			sisu.add(n2);
			sisu.add(n4);
			sisu.add(n11);
			�petus_loetud = false;
			String[] data = new String[m�ngude_arv];
			Integer[] ajad_data = new Integer[m�ngude_arv];
			String[] v�rvid_data = new String[m�ngude_arv];
			JList<String> n5 = new JList<String>(data);

			// peats�kkel
			while (true){
				try {
					mainclapper.run_kinect();
				} catch (IOException e) {
					e.printStackTrace();
				}
				rl = sisu.getWidth();
				rp = sisu.getHeight();

				// Vastavalt akna suurusele suurused
				n1.setBounds(0, 0, rl/10*8, rp/10*3);
				n2.setBounds((int)(rl/10*4.5), rp/10*8, rl/10*2, rp/10*1);
				n3.setBounds(rl/10*0, rp/10*3, rl/10*10, rp/10*2);
				n4.setBounds(rl/10*8, rp/10*0, rl/10*2, rp/10*1);
				n5.setBounds(rl/10*2, rp/10*4, rl/10*7, rp/10*5);
				n3.setFont(new Font("Serif", Font.BOLD, rp/6));
				n11.setBounds(rl/10*1, rp/10*0, rl/10*7, rp/10*1);
				n11.setFont(new Font("Serif", Font.BOLD, rl/25));

				if(koordinaaadid_muutuvad){
					n11.setText(kin_�hendatud);
				}
				else{
					n11.setText(kin_�hendamata);
				}

				// kontrollib, millal muudab tausta
				java.util.Calendar kontroll_aeg1 = java.util.Calendar.getInstance();
				long kontroll_aeg = kontroll_aeg1.getTimeInMillis();

				if (-(prog_k�ima - kontroll_aeg)- vahetamise_kordi*muutumise_aeg   >  muutumise_aeg) {
					kas_koordinaadid_muutuvad();
					
					//muudan k�sitava v�rvi
					Mis_v�rv=v�rvid[v�rv];
					n3.setText(Mis_v�rv);
					// v�ri muudab
					V�rv=suvaline_number();
					sisu.setBackground(V�rvid[V�rv]);

					// kontrollib kas kinect on �hendatud (st muutuvad koordinaadid)
					v�rreldav_kinecti_muutus();

					vahetamise_kordi+=1;
				}
				sisu.repaint();

				// Kinecti jaoks
				if(koordinaaadid_muutuvad){
					if (v�rv == V�rv){
						nupule_vajutatud = mainclapper.getStatus();
					}

				}
				// hiire ja klaviatuuri jaoks
				if (v�rv==V�rv && kas_m��dan_aega){
					java.util.Calendar aeg_k�ima1 = java.util.Calendar.getInstance();
					aeg_k�ima = aeg_k�ima1.getTimeInMillis();
					kas_kuvan_aega = true;
					kas_m��dan_aega = false;
				}

				// lisab reageerimata j�tmise
				if(!kas_m��dan_aega&&!(v�rv==V�rv)&&vahetamise_kordi > 1){
					data[�igeid_vastuseid]= �igeid_vastuseid+1 + ". kord j�tsid REAGEERIMATA :(";
					// V�rv oli  "+  v�rvid[v�rv]+"."
					ajad_data[�igeid_vastuseid]= -1;
					v�rvid_data[�igeid_vastuseid] = v�rvid[v�rv];
					�igeid_vastuseid+=1;
				}

				
				//tagab selle, et kui korra j��b vahele, siis ei kuva eelmist aega			
				if (!(v�rv==V�rv)) {
					kas_m��dan_aega = true;
				}

				//�igete vastuste kuvamine
				if (nupule_vajutatud && (v�rv == V�rv)){
					java.util.Calendar aeg_kinni1 = java.util.Calendar.getInstance();
					aeg_kinni = aeg_kinni1.getTimeInMillis();
					v�rv = suvaline_number();

					// kuvab aja + paneb tulemustesse
					minu_kiirus = aeg_kinni - aeg_k�ima;
					if (kas_kuvan_aega && (minu_kiirus > 0)){
						n3.setText( minu_kiirus +"");
						data[�igeid_vastuseid]= �igeid_vastuseid+1 + ". kord oli reageerimisaeg "+ minu_kiirus + " millisekundit.";
						//v�rv oli " + v�rvid[v�rv]+".";
						ajad_data[�igeid_vastuseid]= (int)minu_kiirus;
						v�rvid_data[�igeid_vastuseid] = v�rvid[v�rv];
						�igeid_vastuseid+=1;
					}
					else{
						//muudan k�sitava v�rvi
						Mis_v�rv=v�rvid[v�rv];
						n3.setText(Mis_v�rv);
					}
					kas_kuvan_aega=false;
					// tagab selle, et kui j��b vahele, siis ei n�ita eelmist aega
					aeg_k�ima=0;
					kas_m��dan_aega = true;
				}	
				nupule_vajutatud = false;

				// kuvab vajadusel, et k�ed on liiga l�hedal
				if(mainclapper.getStatus() && v�rv != V�rv &&koordinaaadid_muutuvad){
					n1.setText(k�ed_l�hedal);
					n1.setFont(new Font("Serif", Font.BOLD, rp/14));
				}
				else{
					n1.setText("");
				}

				// tulemuste kuvamine ja m�ngu l�petamine
				if (l�petan_vajutatud || �igeid_vastuseid > m�ngude_arv-1 || mainclapper.getStatus_lhand_head()&koordinaaadid_muutuvad&!mainclapper.getStatus_rhand_head()){
					n3.setBounds(rl/10*0, rp/10*2, rl/10*10, rp/10*2);
					sisu.add(n5);
					sisu.remove(n1);
					sisu.remove(n2);
					sisu.remove(n4);
					sisu.setBackground(Color.WHITE);

					// Kuvab keskmise
					int mitu=0;
					int summa = 0;
					for (int i = 0; i < �igeid_vastuseid; i++) {
						if (ajad_data[i]!=-1){
							summa += ajad_data[i];
							mitu +=1;
						}
					}
					try{
						double keskmine =(double)summa/(double)mitu;
						n3.setText("AVG: " + keskmine);}
					catch(java.lang.ArithmeticException e){
						System.out.println("jagasid nulliga");
					}

					/// kirjutab faili
					long m�ng_l_ms = m�ng_l�bi_aeg.getTimeInMillis();
					pw.println(m�ng_l_ms);
					for(int i= 0; i<m�ngude_arv; i++){
						pw.print(ajad_data[i] + ";");
						pw.println
						(v�rvid_data[i] );
					}
					pw.println("-----------------------------");
					pw.close();

					break;
				}
			}

			sisu.add(n9);
			sisu.add(n10);
			n9.setText("Uus m�ng");
			
			// V�imalus alustada uut m�ngu + tulemused
			while(true){
				v�rreldav_kinecti_muutus();
				if(koordinaaadid_muutuvad){
					n11.setText(kin_�hendatud);
				}
				else{
					n11.setText(kin_�hendamata);
				}
				rl = sisu.getWidth();
				rp = sisu.getHeight();
				n3.setFont(new Font("Serif", Font.BOLD, rp/7));
				n3.setBounds(rl/10*0, rp/10*2, rl/10*10, rp/10*2);
				n5.setBounds(rl/10*2, rp/10*4, rl/10*7, rp/10*5);
				n9.setBounds(rl/10*3, rp/10*9, rl/10*2, rp/10*1);
				n10.setBounds(rl/10*5, rp/10*9, rl/10*2, rp/10*1);
				n11.setBounds(rl/10*1, rp/10*0, rl/10*7, rp/10*1);
				n11.setFont(new Font("Serif", Font.BOLD, rl/25));
				sisu.repaint();
				try {
					mainclapper.run_kinect();
				} catch (IOException e) {
					e.printStackTrace();
				}
				�petus_loetud = !mainclapper.getStatus_lhand_head() & mainclapper.getStatus_rhand_head()& koordinaaadid_muutuvad;
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				try {
					mainclapper.run_kinect();
				} catch (IOException e1) {
					e1.printStackTrace();
				}

				kas_koordinaadid_muutuvad();
				if (�petus_loetud ){
					if(koordinaaadid_muutuvad){
						n11.setText(kin_�hendatud);
					}
					else{
						n11.setText(kin_�hendamata);
					}
					sisu.remove(n5);
					sisu.remove(n9);
					sisu.remove(n10);
					n3.setText("Alustame");
					sisu.repaint();
					l�petan_vajutatud=false;
					�igeid_vastuseid=0;
					vahetamise_kordi=0;
					break;
				}
			}
		}
	}
}




