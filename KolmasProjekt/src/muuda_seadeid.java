import java.awt.Container;
import java.awt.event.*;

import javax.swing.*;

class muuda_seadeid implements ActionListener {
	
	public void actionPerformed(ActionEvent e) {
		final JFrame raam = new JFrame("Seaded"); // raami loomine
		raam.setSize(300,200); // 

		//	Raami layout
		raam.setLocation(780, 50);
		Container sisu = raam.getContentPane(); // konteineri loomine
		sisu.setLayout(null);
		raam.setVisible(true);
		raam.setResizable(false);
		
		JLabel info = new JLabel("Vali v�rvide vahetamise kiirus(millisekundites):");
		
		JLabel m�ngud = new JLabel("Vali m�ngitavate m�ngude arv: ");
		
		JComboBox <String> kiirus = new JComboBox <String> ();
		kiirus.addItem("500");
		kiirus.addItem("750");
		kiirus.addItem("1000");
		kiirus.addItem("1500");
	    kiirus.addItem("3000");
	    kiirus.addItem("5000");
	    
	    JComboBox <String> M�ngud = new JComboBox <String> ();
	    for (int i = 1; i < 31; i++) {
	    	M�ngud.addItem(i+"");
	    }
	    
	    JButton kinni = new JButton("OK");
	    
	    sisu.add(kiirus);
	    sisu.add(info);
	    sisu.add(kinni);
	    sisu.add(m�ngud);
	    sisu.add(M�ngud);
	    
	    info.setBounds(10, 10, 300, 11);
	    kiirus.setBounds(75, 30, 150, 25);
	    m�ngud.setBounds(10, 70, 300, 13);
	    M�ngud.setBounds(75, 90, 150, 25);
	    kinni.setBounds(112, 130, 75, 25);
	    
	    kiirus.addItemListener(new ComboBoxListener());
	    M�ngud.addItemListener(new ComboBoxListener2());
	    kinni.addActionListener(new ActionListener() {
	        public void actionPerformed(ActionEvent evt) {
	        	raam.dispose();
	        }
	    });
	}
}
